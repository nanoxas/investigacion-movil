package com.example.gabriel.nqueens;

import android.app.Application;

import edu.puc.astral.CloudManager;

/**
 * Created by jose on 11/17/15.
 */
public class MainApplication extends Application {
    private static final String HOST_IP = "http://146.155.115.99";
    private static final String SENDER_ID = "757708555907";

    @Override
    public void onCreate() {
        super.onCreate();

        CloudManager.initialize(this, SENDER_ID, HOST_IP);
    }
}
